function createCard(name, description, pictureUrl, usableStart, usableEnd, location) {
    return `
    <div class="card" style="flex-wrap: wrap; border: 0px;">
          
        <div class="shadow-sm p-3 mb-5 bg-body rounded">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">Starts: ${usableStart}</br> Ends: ${usableEnd}
            </div>
        </div>

    </div>
    `;
  }

  window.addEventListener('DOMContentLoaded', async () => {
      
      const url = 'http://localhost:8000/api/conferences/';
      
      try {
          const response = await fetch(url);
          
          if (!response.ok) 
          {
              // Figure out what to do when the response is bad
          } 
          else 
            {
                const data = await response.json();
                const cardsDiv = document.querySelector('div#cards');
                let columnNum = 0;
                for (let conference of data.conferences) 
                {
                    const detailUrl = `http://localhost:8000${conference.href}`;
                    const detailResponse = await fetch(detailUrl);
                    if (detailResponse.ok) {
                        const details = await detailResponse.json();
                        console.log(details);
                        const name = details.conference.name;
                        const description = details.conference.description;
                        const pictureUrl = details.conference.location.picture_url;
                        const starts = details.conference.starts;
                        const ends = details.conference.ends;
                        const location = details.conference.location.name;
                        let usableStart = new Date(starts);
                        usableStart = usableStart.toDateString();
                        let usableEnd = new Date(ends);
                        usableEnd = usableEnd.toDateString(); 
                        const html = createCard(name, description, pictureUrl, usableStart, usableEnd, location);
                        const columns = document.querySelectorAll('.col');                        
                        let column = columns[columnNum];
                        columnNum += 1;
                        if (columnNum > 2)
                        {
                            columnNum = 0;
                        }
                        column.innerHTML += html;
                    }
                }
            }
    } catch (e) 
    {
      // Figure out what to do if an error is raised
    }
  
});