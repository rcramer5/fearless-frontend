function locationOptions(name, id)
{
    return `<option value="${id}">${name}</option>`
}



window.addEventListener('DOMContentLoaded', async () => 
{
    const url = "http://localhost:8000/api/locations"
    
    const response = await fetch(url)

    if (response.ok)
    {
        const data = await response.json()
        const selectTag = document.getElementById('location')
        for (let location of data.locations)
        {
            const locationData = document.getElementById('option')
            const name = location.name
            const id = parseInt(location.href.replace(/[^0-9\.]/g, ''), 10);
            console.log(name, id)
            let optionTag = locationOptions(name, id)
            selectTag.innerHTML += optionTag

        }
    }
    const formTag = document.getElementById('create-conference-form')
    formTag.addEventListener('submit', async(event) => {
        event.preventDefault()
        const formData = new FormData(formTag)
        const json = JSON.stringify(Object.fromEntries(formData))
        const conferenceUrl = 'http://localhost:8000/api/conferences/'
        const fetchConfig = 
        {
            method: "post",
            body: json,
            headers: 
            {
                'Content-Type': 'application/json',
            },
        }
        const response2 = await fetch(conferenceUrl, fetchConfig);
        if (response2.ok) 
        {
            formTag.reset()
            const newConference = await response2.json()
            console.log(newConference)
            
        }
    })
})