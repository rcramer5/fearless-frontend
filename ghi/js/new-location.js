function stateOptions(name, abbrev)
{
    return `<option value="${abbrev}">${name}</option>`
}



window.addEventListener('DOMContentLoaded', async () => 
{
    const url = "http://localhost:8000/api/states"
    
    const response = await fetch(url)

    if (response.ok)
    {
        const data = await response.json()
        const selectTag = document.getElementById('state')
        for (let state of data.states)
        {
            const stateData = document.getElementById('option')
            const name = state.name
            const abbrev = state.abbreviation
            let optionTag = stateOptions(name, abbrev)
            selectTag.innerHTML += optionTag

        }
    }
    const formTag = document.getElementById('create-location-form')
    formTag.addEventListener('submit', async(event) => 
    {
        event.preventDefault()
        const formData = new FormData(formTag)
        const json = JSON.stringify(Object.fromEntries(formData))
        const locationUrl = 'http://localhost:8000/api/locations/'
        const fetchConfig = 
        {
            method: "post",
            body: json,
            headers: 
            {
                'Content-Type': 'application/json',
            },
        }
        const response2 = await fetch(locationUrl, fetchConfig);
        if (response2.ok) 
        {
            formTag.reset()
            const newLocation = await response2.json()
            
        }
    })
})