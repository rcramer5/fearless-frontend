import React from 'react';

class ConferenceForm extends React.Component
{   
    constructor(props) {
        super(props)
        this.state = 
            {
                name: '',
                description: '',
                maxAttendees: '',
                maxPresentations:'',
                starts: '',
                ends: '',
                location:'',
                locations: []

                
            };
        
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleMaxPresentationsChange = this.handleMaxPresentationsChange.bind(this);
        this.handleMaxAttendeesChange = this.handleMaxAttendeesChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleStartsChange = this.handleStartsChange.bind(this);
        this.handleEndsChange = this.handleEndsChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    async handleSubmit(event)
    {
        event.preventDefault()
        const data = {...this.state}
        data.max_attendees = data.maxAttendees
        data.max_presentations = data.maxPresentations
        delete data.maxAttendees;
        delete data.maxPresentations;
        delete data.locations
        const conferenceUrl = 'http://localhost:8000/api/conferences/'
        const fetchConfig = 
        {
            method: "POST",
            body: JSON.stringify(data),
            headers: 
            {
                'Content-Type': 'application/json',
            },
        }
        const response2 = await fetch(conferenceUrl, fetchConfig);
        if (response2.ok) 
        {
        
            const newConference = await response2.json()
            console.log(newConference)

            const cleared = {
                name: '',
                description: '',
                maxPresentations: '',
                maxAttendees: '',
                starts: '',
                ends: '',
                location: 0,

              };
              this.setState(cleared);
    }
}
    handleNameChange(event)
    {
        const value = event.target.value
        this.setState({name: value})
    }
    
    handleDescriptionChange(event)
    {
        const value = event.target.value
        this.setState({description: value})
    }

    handleMaxPresentationsChange(event)
    {
        const value = event.target.value
        this.setState({maxPresentations: value})
    }

    handleMaxAttendeesChange(event)
    {
        const value = event.target.value
        this.setState({maxAttendees: value})
    }
    handleLocationChange(event)
    {
        const value = event.target.value
        console.log(value)       
        this.setState({location: value})
        
    }
    handleStartsChange(event)
    {
        const value = event.target.value
        this.setState({starts: value})
    }
    handleEndsChange(event)
    {
        const value = event.target.value
        this.setState({ends: value})
    }
    




    async componentDidMount() {
        const url = 'http://localhost:8000/api/locations/';
    
        const response = await fetch(url);
    
        if (response.ok) {
          const data = await response.json();
          this.setState({locations: data.locations});
          
        }
      }

      
      

    render()
    {
        return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit= {this.handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange = {this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value = {this.state.name}/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange = {this.handleStartsChange} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" value = {this.state.starts}/>
                <label htmlFor="room_count">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input  onChange = {this.handleEndsChange}placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" value={this.state.ends}/>
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange = {this.handleDescriptionChange }placeholder="Description" required type="text" name="description" id="description" className="form-control" value={this.state.description}/>
                <label htmlFor="description">Description</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange = {this.handleMaxPresentationsChange}  placeholder="Max_presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" value={this.state.maxPresentations}/>
                <label htmlFor="max_presentations">Max Presentations:</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange = {this.handleMaxAttendeesChange} placeholder="max_attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" value={this.state.maxAttendees}/>
                <label htmlFor="max_attendees"> Max Attendees:</label>
              </div>
              <div className="mb-3">
                <select onChange = {this.handleLocationChange} required id="location" name="location" className="form-select" value= {this.state.location}>
                  <option value="">Choose a location</option>
                  {this.state.locations.map(location => {
                        const locationId = parseInt(location.href.replace(/[^0-9\.]/g, ''), 10);
                        return (
                            <option key={location.href} value={locationId}>
                                {location.name}
                            </option>
                        );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
        )
    }
}
export default ConferenceForm;